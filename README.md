# JellyMove
![](img/0514e2.jpg)
Ever wanted to classify jellyfish activity from *in situ* accelerometry and gyroscope data? Wait no longer, today's the day! This repository houses code accompanying "Augmenting biologging with supervised machine learning to study *in situ* behavior of the medusa *Chrysaora fuscescens*" (Fannjiang et al. 2019), and demonstrates how to reproduce all classification results from the paper.

Clara Fannjiang, T. Aran Mooney, Seth Cones, David Mann, K. Alex Shorter, and Kakani Katija. Augmenting biologging with supervised machine learning to study /in situ/ behavior of the medusa /Chrysaora fuscescens/. 2019. /Journal of Experimental Biology/, in press. DOI: 10.1242/jeb.207654.

---

## Getting started

#### Prerequisites

You'll only need Python 2, NumPy, SciPy, and scikit-learn for main functionality. If you want to reproduce the pretty plots in **demo.ipynb**, Matplotlib and seaborn are also needed.

Clone this repository and add to your path, *e.g.* via pip install -e path_to_jellymove.


#### What does what?

- **features**: Contains preprocessed accelerometery and gyroscope features for each pulse-length period of each *in situ* and laboratory deployment. Files in this directory are named \[date\]\[ITAG ID\]\_\[activity|influence\]\_features.[npz|csv], *e.g.* 0514e2_activity_features.csv contains the featurized data for activity (*i.e.* swimming vs. drifting) from the May 14 deployment using tag e2. The data in the .npz and .csv files are identical.

- **feature_descriptions.csv**: Descriptions of the 46 features (see Methods & Materials in paper for more details). 

- **jellymove.py**: The main stuff. Functions for activity and tether-influence classifier training and prediction.

- **demo.ipynb**: Jupyter Notebook demonstrating how to reproduce training the activity classifier. Switch from "Default File Viewer" to "Ipython Notebook" to view properly on bitbucket:
![](img/notebook_viewer.png)

---

## Getting (jelly)moving

