from setuptools import setup
setup(
   name="jellymove",
   version="1.0",
   description="Training behavioral classifiers for jellyfish",
   author="Clara Wong-Fannjiang",
   author_email="clarafy@berkeley.edu",
   install_requires=["numpy", "scipy", "scikit-learn", "matplotlib"],
)