import time

import numpy as np
import scipy as sc

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from sklearn.svm import SVC
from sklearn.metrics import precision_recall_curve, auc

import matplotlib.pyplot as plt

INSITU_IDS = ["042424", "0424e2", "04243c", "051424", "0514e2", "051724", "0517e2", "0517b7"]
LABORATORY_IDS = ["0518e2", "0521e2", "0521b7", "0531b7"]
ACCELEROMETRY_FEATURES = np.hstack([
  np.arange(5),
  np.array([9, 10, 13, 14, 17, 18]),
  np.arange(21, 27),
  np.arange(39, 45)
])
CLASSIFIER_FEATURES = np.arange(45)
MEAN_ODBA_FEATURE = np.array([45])
THRESHOLDS = np.arange(1e-4, 1 - 1e-4, 1e-4)

def plot_precision_and_recall(prec_k, rec_k, lw=5):
  fig, (ax1, ax2, ax3) = plt.subplots(1, 3, figsize=(15, 3), dpi=300);
  for p, r in zip(prec_k, rec_k):
      ax1.plot(THRESHOLDS, r, "-", c="steelblue", linewidth=lw, alpha=0.8);
      ax1.set_xlabel("Decision threshold"); ax1.set_ylabel("Recall");
      ax2.plot(THRESHOLDS, p, "-", c="steelblue", linewidth=lw, alpha=0.8);
      ax2.set_xlabel("Decision threshold");  ax2.set_ylabel("Precision");
      ax3.plot(r, p, "-k", alpha=0.3, linewidth=lw, label="Precision-Recall Curve");
      ax3.set_xlim([0, 1]); ax3.set_ylim([0, 1]);
      ax3.set_xlabel("Recall"); ax3.set_ylabel("Precision");
  avg_rec = np.mean(np.vstack([r[None, :] for r in rec_k]), axis=0)
  avg_prec = np.mean(np.vstack([p[None, :] for p in prec_k]), axis=0)
  ax3.plot(avg_rec, avg_prec, "-k", linewidth=lw + 2, label="Average");
  handles, labels = ax3.get_legend_handles_labels()
  handles = [handles[0], handles[-1]]
  labels = [labels[0], labels[-1]]
  ax3.legend(handles, labels)
  return avg_rec, avg_prec

def pr_curve(truth, pred_probs, thresholds=THRESHOLDS):
  tp_plus_fn = float(np.sum(truth))
  prec_t = np.empty(thresholds.size)
  rec_t = np.empty(thresholds.size)
  for t, thresh in enumerate(thresholds):
    pred = pred_probs > thresh
    tp_plus_fp = float(np.sum(pred))
    tp = sum([1.0 for i, val in enumerate(pred) if val == 1.0 and truth[i] == 1.0])
    prec = 0.0 if tp_plus_fp == 0 else tp / tp_plus_fp
    rec = tp / tp_plus_fn
    prec_t[t] = prec
    rec_t[t] = rec
  return prec_t, rec_t

def get_eer_threshold(pr, thresholds=THRESHOLDS):
  prec_k, rec_k = pr
  prec_kxt = np.vstack([prec[None, :] for prec in prec_k])
  rec_kxt = np.vstack([rec[None, :] for rec in rec_k])
  avg_prec = np.mean(prec_kxt, axis=0)
  avg_rec = np.mean(rec_kxt, axis=0)
  diff = np.abs(avg_rec - avg_prec)
  diff[np.where(avg_prec == 0.0)] = np.inf
  diff[np.where(avg_rec == 0.0)] = np.inf
  diff[np.where(thresholds % 0.1 != 0)] = np.inf
  idx = np.argmin(diff)  # point on PR curve where precision and recall are closest
  prec_se = sc.stats.sem(prec_kxt[:, idx])
  rec_se = sc.stats.sem(rec_kxt[:, idx])
  print("EER threshold: {}. Precision: {:.3f} (SE = {:.3f}). Recall: {:.3f} (SE = {:.3f}).".format(
    thresholds[idx], avg_prec[idx], prec_se, avg_rec[idx], rec_se))
  return thresholds[idx]

def evaluate(truth, pred_probs, verbose=True, decision_threshold=0.5):
  pred = pred_probs > decision_threshold
  acc = np.where(pred == truth)[0].size / float(truth.size)

  # compute precision and recall
  tp = sum([1.0 for i, val in enumerate(pred) if val == 1.0 and truth[i] == 1.0])
  if np.sum(pred) == 0:
    prec = 0.0
  else:
    prec = tp / np.sum(pred)

  if np.sum(truth) == 0:
    rec = 0.0
  else:
    rec = tp / np.sum(truth)

  # compute AUPRC
  p, r, _ = precision_recall_curve(truth, pred_probs)
  auprc = auc(r, p)
  scores = {"auprc": auprc, "prec": prec, "rec": rec, "acc": acc}
  return scores

def load_feature_npzs(npz_filenames):
  X = []
  y = []
  X_unlabeled = []
  print("Reading features...")
  for npz in npz_filenames:
    with open(npz, "r") as f:
      d = np.load(f)

      # annotated data
      try:
        _ = d["X"][0]
        X.append(d["X"])
        y.append(d["y"])
      except:
        print("  {} has no annotated data.".format(npz))
        X.append(np.array([]))
        y.append(np.array([]))

      # unannotated data (only in situ)
      try:
        _ = d["X_unlabeled"][0]
        X_unlabeled.append(d["X_unlabeled"])
      except:
        print("  {} has no unannotated data.".format(npz))
  return X, y, X_unlabeled

def hstack(list_of_arrays):
  if len(list_of_arrays):
    return np.hstack([a for a in list_of_arrays if a.size])
  return []
def vstack(list_of_arrays):
  if len(list_of_arrays):
    return np.vstack([a for a in list_of_arrays if a.size])
  return []

class JellyMoveClassifier:

  def __init__(self, act_deployment_ids, inf_deployment_ids):
    act_feature_npzs = ["features/{}_activity_features.npz".format(i) for i in act_deployment_ids]
    X_act, y_act, X_unk_act = load_feature_npzs(act_feature_npzs)
    self.X_act = X_act
    self.y_act = y_act
    self.X_unk_act = X_unk_act
    self.act_deployment_ids = act_deployment_ids
    self.n_act_samp, self.n_act_feat = self._get_stacked_shape(X_act)
    print("{} annotated and {} unannotated activity periods, {} features.\n".format(
      self.n_act_samp, self._get_stacked_shape(X_unk_act)[0], self.n_act_feat))

    inf_feature_npzs = ["features/{}_influence_features.npz".format(i) for i in inf_deployment_ids]
    X_inf, y_inf, X_unk_inf = load_feature_npzs(inf_feature_npzs)
    self.X_inf = X_inf
    self.y_inf = y_inf
    self.X_unk_inf = X_unk_inf
    self.inf_deployment_ids = inf_deployment_ids
    self.n_inf_samp, self.n_inf_feat = self._get_stacked_shape(X_inf)
    print("{} annotated and {} unannotated influence periods, {} features.\n".format(
      self.n_inf_samp, self._get_stacked_shape(X_unk_inf)[0], self.n_inf_feat))

   # trained models
    self.act_classifier = None
    self.activity_feature_idx = None
    self.activity_pr_curves = None
    self.act_shuffle_idx = None
    self.inf_classifier = None
    self.influence_feature_idx = None
    self.influence_pr_curves = None
    self.inf_shuffle_idx = None

  def _get_stacked_shape(self, list_of_arrays):
    if len(list_of_arrays):
      return sum([a.shape[0] for a in list_of_arrays]), list_of_arrays[0].shape[1]
    return 0, 0

  def sfs_cv(self, model, X, y, k, selected_feat_idx, allowed_features=CLASSIFIER_FEATURES, decision_threshold=0.5):
    """
    Run one iteration of sequential forward selection (i.e. pick one more feature) via cross-validation.

    Inputs:
      model: scikit-learn model with fit() and predict_proba() methods
      X: numpy array of shape n_samples x n_features
      y: numpy array of shape n_samples (binary labels)
      selected_feat_idx: list, indices of features selected so far
      decision_threshold: float, threshold on p(y | x) for classification

    Outputs:
      index of selected feature (int) and its corresponding CV AUPRC (float)
    """
    n_val = int(y.size / float(k))

    # evaluate each potential new feature
    val_score_per_feat = np.zeros([X.shape[1]])
    for feat_idx in allowed_features:
      # skip if already selected this feature
      if feat_idx in selected_feat_idx: # or feat_idx in [0, 46, 47]:
        continue

      proposed_feat_idx = np.array(selected_feat_idx + [feat_idx])
      val_score = 0.0

      # cross-validation to evaluate addition of potential new feature
      for idx in range(k):
        X_train = np.vstack([X[: idx * n_val, proposed_feat_idx],
          X[idx * n_val + n_val :, proposed_feat_idx]])
        X_val = X[idx * n_val : idx * n_val + n_val, proposed_feat_idx]
        y_train = np.hstack([y[: idx * n_val], y[idx * n_val + n_val :]])
        y_val = y[idx * n_val : idx * n_val + n_val]

        model.fit(X_train, y_train)
        pred_prob_nx2 = model.predict_proba(X_val)
        scores = evaluate(y_val, pred_prob_nx2[:, 1], verbose=False, decision_threshold=decision_threshold)
        val_score += scores["auprc"]

      val_score_per_feat[feat_idx] = val_score / float(k)
    best_feat_idx = np.argmax(val_score_per_feat)
    return best_feat_idx, val_score_per_feat[best_feat_idx]

  def shuffle_data(self, X, y):
    """
    Shuffles data randomly.
    """
    shuffle_idx = np.random.permutation(X.shape[0])
    X = X[shuffle_idx, :]
    y = y[shuffle_idx]
    return X, y, shuffle_idx

  def get_model(self, model_str, **kwargs):
    if model_str == "qda":
      model = QuadraticDiscriminantAnalysis(**kwargs)
    elif model_str == "svm":
      model = SVC(**kwargs)
    elif model_str == "lda":
      model = LinearDiscriminantAnalysis(**kwargs)
    return model

  def print_scores(self, scores, fn=lambda x: x, prefix=" ", format_str="{:.2f}"):
    s = "{{}}AUPRC: {}. Precision: {}. Recall: {}. Accuracy: {}.\n".format(*(4 * [format_str]))
    print(s.format(prefix, *[fn(scores[name]) for name in ["auprc", "prec", "rec", "acc"]]))

  def split_train_val(self, X, y, fold_idx, n_val):
    X_train = np.vstack([X[: fold_idx * n_val, :], X[fold_idx * n_val + n_val :, :]])
    X_val = X[fold_idx * n_val : fold_idx * n_val + n_val]
    y_train = np.hstack([y[: fold_idx * n_val], y[fold_idx * n_val + n_val :]])
    y_val = y[fold_idx * n_val : fold_idx * n_val + n_val]
    return X_train, X_val, y_train, y_val

  def train_with_sfs(self, task, model="qda", allowed_features=CLASSIFIER_FEATURES, use_sfs=True, k=5,
    improve_threshold=0.02, decision_threshold=0.5, seed=1234, **kwargs):
    """
    Cross-validation of activity or influence classifier using sequential forward selection.

    Inputs:
      task: string, "activity" or "influence"
      model: string, which sklearn model to train
    """

    if task == "activity":
      np.random.seed(seed)
      X, y, shuffle_idx = self.shuffle_data(vstack(self.X_act), hstack(self.y_act))
      X_unk = vstack(self.X_unk_act)
      n_sample = self.n_act_samp
      n_feat = self.n_act_feat
      self.act_shuffle_idx = shuffle_idx
    elif task == "influence":
      np.random.seed(seed)
      X, y, shuffle_idx = self.shuffle_data(vstack(self.X_inf), hstack(self.y_inf))
      X_unk = vstack(self.X_unk_inf)
      n_sample = self.n_inf_samp
      n_feat = self.n_inf_feat
      self.inf_shuffle_idx = shuffle_idx
    else:
      raise ValueError("Unrecognized task: {}".format(task))
    format_str = "".join((allowed_features.size - 1) * ["{}, "] + ["{}."])

    n1 = np.sum(y)
    print "Training {} classifier on:\n{} {} periods ({:.2f}%), {} {} periods.\n".format(
      task, int(n1), "drifting" if task == "activity" else "influenced", 100.0 * float(n1) / n_sample,
      int(n_sample - n1), "swimming" if task == "activity" else "uninfluenced")

    model = self.get_model(model, **kwargs)
    prec_curves, rec_curves = [], []
    scores_k = {name: np.zeros([k]) for name in ["auprc", "prec", "rec", "acc"]}
    n_out_val = int(n_sample / float(k))

    # nested CV with SFS
    for out_idx in range(k):
      print "Starting fold {} of outer CV.".format(out_idx)
      Xout_train, Xout_val, yout_train, yout_val = self.split_train_val(X, y, out_idx, n_out_val)

      if use_sfs:
        selected_feat_idx = []
        inner_cv_score = [0.0]
        while True:
          # find remaining feature that gives best AUPRC with inner CV
          best_feat_idx, best_val_score = self.sfs_cv(model, Xout_train, yout_train, k, selected_feat_idx,
            allowed_features=allowed_features, decision_threshold=decision_threshold)

          # decide whether to include this feature, or terminate feature selection
          if best_val_score >= inner_cv_score[-1] + improve_threshold:
            selected_feat_idx += [best_feat_idx]
            inner_cv_score += [best_val_score]
            print " Selecting feature {}, improving inner CV AUPRC from {:.2f} to {:.2f}.".format(best_feat_idx, *inner_cv_score[-2 :])
          else:
            print " Terminating feature selection after {} features.".format(len(selected_feat_idx))
            break
        selected_feat_idx = np.array(selected_feat_idx)
      else:
        print("Using features: " + format_str.format(*allowed_features))
        selected_feat_idx = np.copy(allowed_features)

      # with final selected features, train and evaluate on outer train and test set
      model.fit(Xout_train[:, selected_feat_idx], yout_train)
      pred_probs_nx2 = model.predict_proba(Xout_val[:, selected_feat_idx])
      scores = evaluate(yout_val, pred_probs_nx2[:, 1], decision_threshold=decision_threshold)

      # record and report outer test scores
      for name in scores:
        scores_k[name][out_idx] = scores[name]
      prec_t, rec_t = pr_curve(yout_val, pred_probs_nx2[:, 1], np.arange(1e-4, 1 - 1e-4, 1e-4))
      prec_curves.append(prec_t)
      rec_curves.append(rec_t)
      print "Predictions on outer validation set (with decision threshold {}):".format(decision_threshold)
      self.print_scores(scores)

    # report average over all outer test scores
    print "Average outer validation (with decision threshold {}):".format(decision_threshold)
    self.print_scores(scores_k, fn=lambda arr: np.mean(arr), format_str="{:.3f}")
    self.print_scores(scores_k, fn=lambda arr: np.std(arr, ddof=1) / np.sqrt(k),
      prefix="Standard errors:\n ", format_str="{:.3f}")

    # final feature selection
    if use_sfs:
      print "Selecting final feature subset via CV on entire dataset."
      selected_feat_idx = []
      cv_score = [0.0]
      while True:
        best_feat_idx, best_val_score = self.sfs_cv(model, X, y, k, selected_feat_idx, decision_threshold=decision_threshold)

        # decide whether to take this feature, or terminate feature selection
        if best_val_score >= cv_score[-1] + improve_threshold:
          selected_feat_idx += [best_feat_idx]
          cv_score += [best_val_score]
          print " Selecting feature {}, improving CV score from {:.2f} to {:.2f}.".format(best_feat_idx, *cv_score[-2 :])
        else:
          print " Terminating feature selection after {} features.".format(len(selected_feat_idx))
          break
      selected_feat_idx = np.array(selected_feat_idx)
    else:
      selected_feat_idx = np.copy(allowed_features)

    model.fit(X[:, selected_feat_idx], y)
    if task == "activity":
      self.act_classifier = model
      self.activity_feature_idx = selected_feat_idx
      self.activity_pr_curves = (prec_curves, rec_curves)
    elif task == "influence":
      self.inf_classifier = model
      self.influence_feature_idx = selected_feat_idx
      self.influence_pr_curves = (prec_curves, rec_curves)
    return model, selected_feat_idx, (prec_curves, rec_curves)

  def _percent(self, y, dnm=None):
    if dnm is not None:
      return 100.0 * np.sum(y) / float(dnm)
    return 100.0 * np.sum(y) / y.size

  def predict(self, inf_threshold=0.5, act_threshold=0.5):
    """
    Predict influence and activity of unannotated periods.

    Inputs:
      inf_threshold: float, decision threshold for influence classifier
      act_threshold: float, decision threshold for activity classifier
    """
    if self.act_classifier is None or self.inf_classifier is None:
      print("Train classifiers first (run train_with_sfs).")
      return

    for X_unk_act, X_unk_inf, dep_id in zip(self.X_unk_act, self.X_unk_inf, self.act_deployment_ids):
      if X_unk_act.size:  # laboratory deployments don't have unannotated data
        inf_predprob_tx2 = self.inf_classifier.predict_proba(X_unk_inf[:, self.influence_feature_idx])
        act_predprob_tx2 = self.act_classifier.predict_proba(X_unk_act[:, self.activity_feature_idx])
        inf_pred = inf_predprob_tx2[:, 1]>= inf_threshold
        act_pred = act_predprob_tx2[:, 1] >= act_threshold
        noinf_pred = ~inf_pred
        act_noinf_pred = act_pred.astype(bool) & noinf_pred.astype(bool)

        print("Deployment {}:\n  Periods predicted as influenced: {} out of {} ({:.2f}%).".format(
          dep_id, np.sum(inf_pred), inf_pred.size, self._percent(inf_pred)))
        print("  Periods predicted as drifting: {} out of {} ({:.2f}%).".format(
          np.sum(act_noinf_pred), np.sum(noinf_pred), self._percent(act_noinf_pred, dnm=np.sum(noinf_pred))))

def predict_in_situ_activity(act_classifier, act_feature_idx, decision_threshold=0.5):
  act_feature_npzs = ["features/{}_activity_features.npz".format(i) for i in INSITU_IDS]
  X_act, y_act, _ = load_feature_npzs(act_feature_npzs)
  predprob = []
  for name, X, y in zip(INSITU_IDS, X_act, y_act):
    if y.size:
      predprob_nx2 = act_classifier.predict_proba(X[:, act_feature_idx])
      predprob.append(predprob_nx2[:, 1])
      scores = evaluate(y, predprob_nx2[:, 1], decision_threshold=decision_threshold)
      print("Predictions on {}:\n  Accuracy: {:.3f}. Precision: {:.3f}. Recall: {:.3f}.".format(
        name, scores["acc"], scores["prec"], scores["rec"]))
  # total statistics
  y = hstack(y_act)
  predprob = hstack(predprob)
  scores = evaluate(y, predprob, decision_threshold=decision_threshold)
  print("Total:\n  Accuracy: {:.3f}. Precision: {:.3f}. Recall: {:.3f}.".format(
    scores["acc"], scores["prec"], scores["rec"]))


